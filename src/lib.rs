use clap::{App, Arg};
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

#[derive(Debug)]
pub struct Config {
    files: Vec<String>,
    number_lines: bool,
    number_nonblank_lines: bool,
}

type MyResult<T> = Result<T, Box<dyn Error>>;

pub fn get_args() -> MyResult<Config> {
    let matches = App::new("catr")
        .version("0.1.0")
        .author("Tim Chernyavskiy <timofey.chernyavskiy@gmail.com>")
        .about("Rust cat")
        .arg(
            Arg::with_name("files")
                .value_name("FILE")
                .help("Input file(s)")
                .multiple(true)
                .default_value("-"),
        )
        .arg(
            Arg::with_name("number")
                .help("Number lines")
                .short("n")
                .long("number")
                .conflicts_with("number_nb_lines")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("number_nonblank")
                .help("Number nonblank lines")
                .short("b")
                .long("number-nonblank")
                .takes_value(false),
        )
        .get_matches();

    Ok(Config {
        files: matches.values_of_lossy("files").unwrap(),
        number_lines: matches.is_present("number"),
        number_nonblank_lines: matches.is_present("number_nonblank"),
    })
}

pub fn run(config: Config) -> MyResult<()> {
    for filename in config.files {
        match open(&filename) {
            Err(err) => eprintln!("Failed to open {}: {}", filename, err),
            Ok(buf) => {
                let mut lc = 0;
                for line in buf.lines() {
                    match (config.number_lines, config.number_nonblank_lines) {
                        (false, false) => println!("{}", line?),
                        (true, false) => println!("{:>6}\t{}", { lc += 1; lc }, line?),
                        (false, true) => {
                            let text = line?;
                            if text.is_empty() {
                                println!();
                            } else {
                                println!("{:>6}\t{}", { lc += 1; lc }, text);
                            }
                        },
                        (_,_) => unimplemented!(),
                    };
                };
            },
        }
    }
    Ok(())
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}
